Name:           pcsc-lite
Version:        2.0.0
Release:        1
Summary:        Middleware to access a smart card using SCard API (PC/SC)
License:        BSD
URL:            https://pcsclite.apdu.fr/
Source0:        https://pcsclite.apdu.fr/files/%{name}-%{version}.tar.bz2

BuildRequires:  systemd-devel polkit-devel gettext-devel
BuildRequires:  perl-podlators doxygen gnupg2 gcc python3 flex

Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Requires:       pcsc-ifd-handler polkit
Recommends:     ccid

Provides:       pcsc-lite-libs%{?_isa} pcsc-lite-libs
Obsoletes:      pcsc-lite-libs

%description
PC/SC Lite is a middleware to access a smart card using SCard API (PC/SC).
This package contains PC/SC Lite server and other utilities.

%package        devel
Summary:        PC/SC Lite library and header files
Requires:       python3
Requires:       %{name} = %{version}-%{release}

%description    devel
This package includes PC/SC Lite library and header files for development.

%package        help
Summary:        Documentation for PC/SC Lite
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Requires:       man
Provides:       %{name}-doc
Obsoletes:      %{name}-doc

%description    help
This package includes documentation for PC/SC Lite.

%prep
%autosetup -n %{name}-%{version} -p1

for file in ChangeLog; do
    iconv -f ISO-8859-1 -t UTF-8 -o $file.new $file && \
    touch -r $file $file.new && \
    mv $file.new $file
done


%build
%configure \
  --disable-static \
  --enable-polkit \
  --enable-usbdropdir=%{_libdir}/pcsc/drivers
%make_build
doxygen doc/doxygen.conf
rm -f doc/api/*.{map,md5}


%install
%make_install

mkdir -p %{buildroot}/%{_sysconfdir}/reader.conf.d
mkdir -p %{buildroot}/%{_libdir}/pcsc/drivers
mkdir -p %{buildroot}/%{_localstatedir}/run/pcscd

%post
%systemd_post pcscd.socket pcscd.service

%preun
%systemd_preun pcscd.socket pcscd.service

%postun
%systemd_postun_with_restart pcscd.socket pcscd.service

%ldconfig_scriptlets

%files
%doc AUTHORS ChangeLog HELP README SECURITY TODO
%doc doc/README.polkit
%doc install_spy.sh uninstall_spy.sh
%license COPYING
%dir %{_sysconfdir}/reader.conf.d/
%dir %{_libdir}/pcsc/
%dir %{_libdir}/pcsc/drivers/
%ghost %dir %{_localstatedir}/run/pcscd/
%{_datadir}/polkit-1/actions/org.debian.pcsc-lite.policy
%{_libdir}/libpcsclite.so.*
%{_sbindir}/pcscd
%{_unitdir}/pcscd*
%exclude %{_libdir}/*.la
%exclude %{_docdir}/pcsc-lite/README.DAEMON

%files devel
%{_bindir}/pcsc-spy
%{_includedir}/PCSC/
%{_libdir}/libpcsclite.so
%{_libdir}/libpcscspy.so*
%{_libdir}/pkgconfig/*.pc


%files help
%doc doc/api/ doc/example/pcsc_demo.c
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*


%changelog
* Tue Feb 6 2024 tangyuchen <tangyuchen5@huawei.com> - 2.0.0-1
- update to 2.0.0
  - Adjust USB drivers path at run-time via environment variable PCSCLITE_HP_DROPDIR
  - Add '--disable-polkit' option
  - Reset eventCounter when a reader is removed
  - Add "polkit" in "pcscd -v" output if enabled
  - Doxygen: document SCARD_E_INVALID_VALUE for some functions
  - use secure_getenv(3) if available
  - Some other minor improvements

* Sun Jan 29 2023 liusirui <liusirui@huawei.com> - 1.9.9-1
- update to 1.9.9

* Thu Oct 20 2022 liusirui <liusirui@huawei.com> - 1.9.4-2
- backport patch to fix data race

* Tue Nov 23 2021 yanglongkang <yanglongkang@huawei.com> - 1.9.4-1
- update to 1.9.4

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.9.0-3
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Oct 29 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 1.9.0-2
- backport some patches to solve some upstream problems

* Tue Jul 21 2020 jixinjie <jixinjie@huawei.com> - 1.9.0-1
- update package to 1.9.0

* Tue Feb 18 2020 Shijie Luo <buildteam@openeuler.org> - 1.8.23-9
- change to use python3 for pcsc-spy python script.

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.23-8
- Change the recommends of pcsc-lite-ccid to ccid

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.23-7
- Repackage

* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.23-6
- Fix postun in spec file

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.23-5
- Package init

